# https://github.com/andreyorst/plug.kak
source "%val{config}/plugins/plug.kak/rc/plug.kak"

# NOTE: kak-lsp is installed via pacman
# https://github.com/ul/kak-lsp
#plug "ul/kak-lsp" do %{
#    cargo build --release --locked
#    cargo install --force
#}

# https://github.com/Delapouite/kakoune-buffers
plug "Delapouite/kakoune-buffers"

# https://github.com/TeddyDD/kakoune-edit-or-dir
plug "TeddyDD/kakoune-edit-or-dir"

# https://github.com/occivink/kakoune-sudo-write
plug "occivink/kakoune-sudo-write"

# https://github.com/ABuffSeagull/kakoune-discord
#plug "abuffseagull/kakoune-discord" do %{ cargo install --path . --force } %{
#      discord-presence-enable
#}

# https://github.com/antekmeco/kakoune-discord
#plug "antekmeco/kakoune-discord" do %{ cargo install --path . --force } %{
#      discord-presence-enable
#}

# https://github.com/natsukagami/kakoune-discord
plug "natsukagami/kakoune-discord" do %{ cargo install --path . --force } %{
      discord-presence-enable
}

# https://github.com/jwhett/boxes-kak
#plug "jwhett/boxes-kak"

# https://github.com/jwhett/figlet-kak
#plug "jwhett/figlet-kak"

# https://github.com/jwhett/cowsay-kak
#plug "jwhett/cowsay-kak"i

# enable language server
eval %sh{kak-lsp --kakoune -s $kak_session}
hook global WinSetOption filetype=(rust|python|go|javascript|typescript|c|cpp|bash|sh) %{
  lsp-enable-window
  lsp-auto-hover-enable
  lsp-auto-signature-help-enable
}

# add line at column 80
add-highlighter global/ column 80 black,red
